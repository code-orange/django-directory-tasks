from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_prefill_mail,
)


class Command(BaseCommand):
    help = "Run task: directory_prefill_mail"

    def handle(self, *args, **options):
        directory_prefill_mail()
