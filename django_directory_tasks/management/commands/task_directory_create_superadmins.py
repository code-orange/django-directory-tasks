from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_create_superadmins,
)


class Command(BaseCommand):
    help = "Run task: directory_create_superadmins"

    def handle(self, *args, **options):
        directory_create_superadmins()
