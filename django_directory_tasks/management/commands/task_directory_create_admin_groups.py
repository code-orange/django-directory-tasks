from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_create_admin_groups,
)


class Command(BaseCommand):
    help = "Run task: directory_create_admin_groups"

    def handle(self, *args, **options):
        directory_create_admin_groups()
