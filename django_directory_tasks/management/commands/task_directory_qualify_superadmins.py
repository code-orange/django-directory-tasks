from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_qualify_superadmins,
)


class Command(BaseCommand):
    help = "Run task: directory_qualify_superadmins"

    def handle(self, *args, **options):
        directory_qualify_superadmins()
