from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_normalize_phone,
)


class Command(BaseCommand):
    help = "Run task: directory_normalize_phone"

    def handle(self, *args, **options):
        directory_normalize_phone()
