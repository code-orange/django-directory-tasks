from django.core.management.base import BaseCommand

from django_directory_tasks.django_directory_tasks.tasks import (
    directory_sync_ectisrv_permissions,
)


class Command(BaseCommand):
    help = "Run task: directory_sync_ectisrv_permissions"

    def handle(self, *args, **options):
        directory_sync_ectisrv_permissions()
