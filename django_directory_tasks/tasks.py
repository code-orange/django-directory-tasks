import base64
import time
from copy import deepcopy

import ldap3
import phonenumbers
from celery import shared_task
from ldap3 import MODIFY_REPLACE, MODIFY_ADD, BASE, ALL_ATTRIBUTES, MODIFY_DELETE
from ldap3.core.exceptions import LDAPNoSuchObjectResult
from ldap3.extend.microsoft.addMembersToGroups import ad_add_members_to_groups

from django_directory_api.django_directory_api.api.resources import unpack_guid
from django_directory_main.django_directory_main.ldap_connection import ldap_connection
from django_directory_main.django_directory_main.password import generate_password
from django_directory_models.django_directory_models.models import *
from django_kmi_cloud_panel.django_kmi_cloud_panel.models import CpDomains
from django_kmi_cloud_panel.django_kmi_cloud_panel.views import *
from django_master_data_backend_client.django_master_data_backend_client.func import (
    master_data_backend_get_customer,
)
from django_pwmgr_client.django_pwmgr_client.func import pwmgr_set_single_credentials


@shared_task(name="directory_create_superadmins")
def directory_create_superadmins():
    all_tenants_without_sadmins = DirectoryTenantSettings.objects.filter(
        super_admin__isnull=True, org_tag__isnull=False
    )

    for tenant_without_sadmin in all_tenants_without_sadmins:
        username = tenant_without_sadmin.org_tag + "admin"
        password = generate_password()
        domain = CpDomains.objects.get(
            companycode=tenant_without_sadmin.org_tag, isdefault=1
        ).domain

        print("Would create: " + username)

        user_data = dict()
        user_data["Company"] = ""  # tenant_without_sadmin.customer.name
        user_data["Department"] = "IT"
        user_data["DisplayName"] = username
        user_data["Domain"] = domain
        user_data["Firstname"] = ""
        user_data["GeneratePassword"] = "true"
        user_data["JobTitle"] = ""
        user_data["Lastname"] = ""
        user_data["PasswordNeverExpires"] = "true"
        user_data["Pwd"] = password
        user_data["TelephoneNumber"] = ""
        user_data["Username"] = username
        user_data["confirmPwd"] = password

        new_user_data = post_data_to_api(
            url="/company/" + tenant_without_sadmin.org_tag + "/users",
            data=user_data,
        )

        if "success" in new_user_data:
            time.sleep(10)

            ldap_con = ldap_connection()

            if not ldap_con.search(
                search_base="ou="
                + tenant_without_sadmin.org_tag
                + ","
                + settings.HOSTING_LDAP_SEARCH_BASE,
                search_filter="(&(objectClass=user)(userPrincipalName="
                + username
                + "@"
                + domain
                + "))",
                search_scope=ldap3.SUBTREE,
                attributes=ldap3.ALL_ATTRIBUTES,
                get_operational_attributes=True,
                size_limit=0,
            ):
                raise Exception("Lookup failed.")

            ldap_objects = ldap_con.response

            ldap_user = ldap_objects[0]
            username_sam = ldap_user["attributes"]["sAMAccountName"][0]

            tenant_without_sadmin.super_admin = username_sam
            tenant_without_sadmin.save()
        else:
            continue

    return


@shared_task(name="directory_qualify_superadmins")
def directory_qualify_superadmins():
    all_tenants_with_sadmins = DirectoryTenantSettings.objects.filter(
        super_admin__isnull=False, org_tag__isnull=False
    )

    ldap_con = ldap_connection()

    for tenant_with_sadmin in all_tenants_with_sadmins:
        print(
            "Adding "
            + tenant_with_sadmin.super_admin
            + " to "
            + "Admins@"
            + tenant_with_sadmin.org_tag
        )

        if not ldap_con.search(
            search_base="ou="
            + tenant_with_sadmin.org_tag
            + ","
            + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(sAMAccountName="
            + tenant_with_sadmin.super_admin
            + "))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        superadmin_dn = ldap_objects[0]["dn"]

        if not ldap_con.search(
            search_base="ou="
            + tenant_with_sadmin.org_tag
            + ","
            + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=group)(name=Admins@"
            + tenant_with_sadmin.org_tag
            + "))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        admin_group_dn = ldap_objects[0]["dn"]

        # add user to Admins@$org_tag
        ad_add_members_to_groups(ldap_con, superadmin_dn, admin_group_dn)

    return


@shared_task(name="directory_sync_masterdata")
def directory_sync_masterdata():
    ldap_con = ldap_connection()

    for tenant in DirectoryTenantSettings.objects.filter(org_tag__isnull=False):
        org_tag = tenant.org_tag

        # fetch master data
        customer_md = master_data_backend_get_customer(
            settings.MDAT_ROOT_CUSTOMER_ID, tenant.customer_id
        )

        customer_addr = customer_md["delivery_address"]

        if not ldap_con.search(
            search_base=f"OU={org_tag}," + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(objectCategory=person))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        md_attr = dict()
        md_attr["company"] = tenant.customer.name[:63]
        md_attr["countryCode"] = customer_addr["street"]["city"]["country"]["num_code"]
        md_attr["c"] = customer_addr["street"]["city"]["country"]["iso"]
        md_attr["co"] = customer_addr["street"]["city"]["country"]["title"]
        md_attr["streetAddress"] = (
            customer_addr["street"]["title"] + " " + customer_addr["house_nr"]
        )
        md_attr["l"] = customer_addr["street"]["city"]["title"]
        md_attr["postalCode"] = customer_addr["street"]["city"]["zip_code"]

        for user in ldap_objects:
            print(
                "Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0]
            )

            for ldap_attribute, ldap_value in md_attr.items():
                if ldap_attribute in user["attributes"]:
                    if user["attributes"][ldap_attribute][0] == ldap_value:
                        continue

                ldap_con.modify(
                    user["dn"],
                    {ldap_attribute: [(MODIFY_REPLACE, ldap_value)]},
                )

    return


@shared_task(name="directory_sync_linux")
def directory_sync_linux():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        if "uid" in user["attributes"]:
            if user["attributes"]["uid"][0] == user["attributes"]["sAMAccountName"][0]:
                continue

        ldap_con.modify(
            user["dn"],
            {"uid": [(MODIFY_REPLACE, user["attributes"]["sAMAccountName"][0])]},
        )

        if "ldapPublicKey" not in user["attributes"]["objectClass"]:
            ldap_con.modify(
                user["dn"], {"objectClass": [(MODIFY_ADD, "ldapPublicKey")]}
            )

    return


@shared_task(name="directory_sync_owncloud")
def directory_sync_owncloud():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        if "ownCloudQuota" not in user["attributes"]:
            ldap_con.modify(user["dn"], {"ownCloudQuota": [(MODIFY_ADD, "0 GB")]})

        if "ownCloud" not in user["attributes"]["objectClass"]:
            ldap_con.modify(user["dn"], {"objectClass": [(MODIFY_ADD, "ownCloud")]})

    return


@shared_task(name="directory_sync_mq")
def directory_sync_mq():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        if "mqttUser" not in user["attributes"]["objectClass"]:
            ldap_con.modify(user["dn"], {"objectClass": [(MODIFY_ADD, "mqttUser")]})

        # if 'mqttDevice' not in user['attributes']['objectClass']:
        #     ldap_con.modify(
        #         user['dn'],
        #         {'objectClass': [(MODIFY_ADD, 'mqttDevice')]}
        #     )

        if "mqttSecurity" not in user["attributes"]["objectClass"]:
            ldap_con.modify(user["dn"], {"objectClass": [(MODIFY_ADD, "mqttSecurity")]})

        if "mqttAccountName" not in user["attributes"]:
            ldap_con.modify(
                user["dn"],
                {
                    "mqttAccountName": [
                        (MODIFY_ADD, user["attributes"]["sAMAccountName"][0])
                    ]
                },
            )

        # if 'isEnabled' not in user['attributes']:
        #     ldap_con.modify(
        #         user['dn'],
        #         {'isEnabled': [(MODIFY_ADD, 'TRUE')]}
        #     )

    return


@shared_task(name="directory_sync_ectisrv")
def directory_sync_ectisrv():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        sip_uri = (
            "sip:"
            + user["attributes"]["sAMAccountName"][0].lower()
            + "@intra.srvfarm.net"
        )

        if "ectisrv3SchemaVersion" not in user["attributes"]:
            ldap_con.modify(user["dn"], {"ectisrv3SchemaVersion": [(MODIFY_ADD, "4")]})

        if "ectisrv3UserSipUri" in user["attributes"]:
            if user["attributes"]["ectisrv3UserSipUri"][0] == sip_uri:
                continue

        ldap_con.modify(user["dn"], {"ectisrv3UserSipUri": [(MODIFY_REPLACE, sip_uri)]})

    return


@shared_task(name="directory_sync_ectisrv_permissions")
def directory_sync_ectisrv_permissions():
    ldap_con = ldap_connection()

    for tenant in DirectoryTenantSettings.objects.filter(org_tag__isnull=False):
        org_tag = tenant.org_tag

        if not ldap_con.search(
            search_base=f"OU={org_tag}," + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(objectCategory=person))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        company_cti_ids = list()

        for user in ldap_objects:
            if "ectisrv3UserSipUri" in user["attributes"]:
                company_cti_ids.append(
                    user["attributes"]["ectisrv3UserSipUri"][0].lower()
                )

        for user in ldap_objects:
            print(
                "Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0]
            )

            # skip if own user has no sip uri
            if "ectisrv3UserSipUri" not in user["attributes"]:
                continue

            company_cti_ids_copy = deepcopy(company_cti_ids)

            new_permissions = list()

            # remove existing permission entries from list
            for company_cti_id in company_cti_ids:
                # do not add permissions for self
                if (
                    company_cti_id.lower()
                    == user["attributes"]["ectisrv3UserSipUri"][0].lower()
                ):
                    company_cti_ids_copy.remove(company_cti_id)
                    continue

                # check if entry is present and do not modify it
                if "ectisrv3UserPermissions" in user["attributes"]:
                    for cti_user_permission_entry in user["attributes"][
                        "ectisrv3UserPermissions"
                    ]:
                        if cti_user_permission_entry.lower().startswith(
                            company_cti_id.lower() + ";"
                        ):
                            company_cti_ids_copy.remove(company_cti_id)

            # add new permission entries from list
            for company_cti_id in company_cti_ids_copy:
                new_permissions.append(
                    f"{company_cti_id};0X00018001;0X00000000;0X00000000§"
                )

            if len(new_permissions) > 0:
                ldap_con.modify(
                    user["dn"],
                    {"ectisrv3UserPermissions": [(MODIFY_ADD, new_permissions)]},
                )

    return


@shared_task(name="directory_sync_org_tag_attribute")
def directory_sync_org_tag_attribute():
    reserved_names = list()
    reserved_names.append("Applications")
    reserved_names.append("Clients")
    reserved_names.append("Exchange")
    reserved_names.append("Servers")
    reserved_names.append("RDS")
    reserved_names.append("Mobile")

    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        dn_list = ldap3.utils.dn.parse_dn(user["dn"])
        dn_index = 1

        while True:
            org_tag = dn_list[dn_index][1]
            dn_index += 1
            if org_tag not in reserved_names:
                break

        if "extensionAttribute1" in user["attributes"]:
            if user["attributes"]["extensionAttribute1"][0] == org_tag:
                continue

        ldap_con.modify(
            user["dn"], {"extensionAttribute1": [(MODIFY_REPLACE, org_tag)]}
        )

    return


@shared_task(name="directory_sync_immutable_id")
def directory_sync_immutable_id():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        user_guid = unpack_guid(user["attributes"]["objectGUID"][0])
        user_guid_b64 = base64.b64encode(user_guid.encode("utf-8"))

        if "codeorange-immutable-id" in user["attributes"]:
            if user["attributes"]["codeorange-immutable-id"][0] == user_guid_b64:
                continue

        ldap_con.modify(
            user["dn"], {"codeorange-immutable-id": [(MODIFY_REPLACE, user_guid_b64)]}
        )

    return


@shared_task(name="directory_sync_mdat_to_tenants")
def directory_sync_mdat_to_tenants():
    known_tenants = DirectoryTenantSettings.objects.all()
    known_customer_ids = list()

    for known_tenant in known_tenants:
        known_customer_ids.append(known_tenant.customer.id)

    new_mdat_customers = MdatCustomers.objects.filter(enabled=True).exclude(
        id__in=known_customer_ids
    )

    for new_mdat_customer in new_mdat_customers:
        new_mdat_customer_tenant = DirectoryTenantSettings(
            customer=new_mdat_customer,
            org_tag=new_mdat_customer.org_tag,
            super_admin=None,
        )
        new_mdat_customer_tenant.save()

    return


@shared_task(name="directory_sync_cloudpanel_auth")
def directory_sync_cloudpanel_auth():
    new_tenants = DirectoryTenantSettings.objects.filter(org_tag__isnull=True)

    for new_tenant in new_tenants:
        tenant_data = {
            "AdminEmail": new_tenant.customer.primary_email,
            "AdminName": new_tenant.customer.name,
            "City": "",
            "CompanyCode": "",
            "CompanyName": new_tenant.customer.name,
            "Country": "DE",
            "DomainName": str(
                "t"
                + str(new_tenant.customer.id)
                + "."
                + settings.HOSTING_DEFAULT_DOMAIN_BASE
            ).lower(),
            "OrgPlanID": 1,
            "PhoneNumber": "",
            "Reference": new_tenant.customer.id,
            "State": "",
            "Street": "",
            "Website": "",
            "ZipCode": "",
            "resellerCode": "",
        }

        new_tenant_response = post_data_to_api("/companies", tenant_data)

        new_tenant.org_tag = new_tenant_response["company"]["companyCode"]
        new_tenant.save()

    return


@shared_task(name="directory_sync_default_hosting_domain")
def directory_sync_default_hosting_domain():
    all_tenants = DirectoryTenantSettings.objects.all()

    for tenant in all_tenants:
        try:
            hosting_domain = CpDomains.objects.get(
                companycode=tenant.org_tag,
                domain=str(
                    "t"
                    + str(tenant.customer.id)
                    + "."
                    + settings.HOSTING_DEFAULT_DOMAIN_BASE
                ).lower(),
            )
        except CpDomains.DoesNotExist:
            domain_data = {
                "DomainName": str(
                    "t"
                    + str(tenant.customer.id)
                    + "."
                    + settings.HOSTING_DEFAULT_DOMAIN_BASE
                ).lower(),
            }

            new_domain_response = post_data_to_api(
                "/company/" + tenant.org_tag + "/domains", domain_data
            )

    return


@shared_task(name="directory_create_computer_containers")
def directory_create_computer_containers():
    ldap_con = ldap_connection()

    all_tenants = DirectoryTenantSettings.objects.all()

    for tenant in all_tenants:
        tenant_ou = "ou=" + tenant.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE

        try:
            ldap_con.add("ou=Clients," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=Mobile,ou=Clients," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=Servers," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=RDS,ou=Servers," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=VZ,ou=Servers," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=MessageQueuing," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add(
                "ou=Clients,ou=MessageQueuing," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add(
                "ou=Groups,ou=MessageQueuing," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add("ou=DNS-Zones," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=DHCP," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=Zones,ou=DHCP," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=Telephony," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add(
                "ou=SIP-Trunks,ou=Telephony," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add(
                "ou=IAX-Trunks,ou=Telephony," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add("ou=CloudPBX,ou=Telephony," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add(
                "ou=Endpoints,ou=CloudPBX,ou=Telephony," + tenant_ou,
                "organizationalUnit",
            )
        except:
            pass

        try:
            ldap_con.add("ou=RADIUS," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=WAN," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add("ou=Credentials,ou=WAN," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add(
                "ou=Network-Access-Servers,ou=WAN," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add(
                "ou=Physical-Access-Control," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add("ou=Remote-Session-Access," + tenant_ou, "organizationalUnit")
        except:
            pass

        try:
            ldap_con.add(
                "ou=Locks,ou=Physical-Access-Control," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

        try:
            ldap_con.add(
                "ou=Cards,ou=Physical-Access-Control," + tenant_ou, "organizationalUnit"
            )
        except:
            pass

    return


@shared_task(name="directory_create_admin_groups")
def directory_create_admin_groups():
    ldap_con = ldap_connection()

    all_tenants = DirectoryTenantSettings.objects.all()

    obj_class = "group"

    admin_group_names = (
        "DIRECTORY",
        "DOMAINS",
        "DNS",
        "WWW",
        "UC",
        "EMAIL",
        "COMPUTECLOUD",
        "CMDB",
        "BROADBAND",
        "TELEPHONY",
        "VAR",
        "MQ",
        "PBXSELFSERVICE",
        "MARKETPLACE",
        "SCM",
        "WLAN",
        "PKI",
        "NETWORK",
        "SECURITY",
        "CTI",
    )

    for tenant in all_tenants:
        tenant_ou = "ou=" + tenant.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE

        for admin_group_name in admin_group_names:
            group_cn = admin_group_name.upper() + "_Admins@" + tenant.org_tag.upper()
            group_name = group_cn.replace("@", "_")

            try:
                group_dn = "cn=" + group_cn + "," + tenant_ou

                attributes = {
                    "cn": group_cn,
                    "name": group_name,
                    "displayName": group_cn,
                    "sAMAccountName": group_name,
                    "description": group_name,
                }

                ldap_con.add(group_dn, obj_class, attributes)
            except:
                pass

    return


@shared_task(name="directory_sync_password_entry")
def directory_sync_password_entry():
    ldap_con = ldap_connection()

    if not ldap_con.search(
        search_base=settings.HOSTING_LDAP_SEARCH_BASE,
        search_filter="(&(objectClass=user)(objectCategory=person))",
        search_scope=ldap3.SUBTREE,
        attributes=ldap3.ALL_ATTRIBUTES,
        get_operational_attributes=True,
        size_limit=0,
    ):
        raise Exception("Lookup failed.")

    ldap_objects = ldap_con.response

    for user in ldap_objects:
        print("Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0])

        user_attributes = user["attributes"]

        # check extensionAttribute1 / org_tag
        if "extensionAttribute1" in user_attributes:
            if (
                user_attributes["extensionAttribute1"][0] is None
                or user_attributes["extensionAttribute1"][0] == ""
            ):
                continue
        else:
            continue

        org_tag = user_attributes["extensionAttribute1"][0]

        description = str()
        description += "Display Name: " + user_attributes["displayName"][0] + "\n"
        description += (
            "Distinguished Name (DN): " + user_attributes["distinguishedName"][0] + "\n"
        )
        description += "Common Name (CN): " + user_attributes["cn"][0] + "\n"
        description += (
            "User Principal Name (UPN): "
            + user_attributes["userPrincipalName"][0]
            + "\n"
        )

        if "mail" in user_attributes:
            description += "Mail: " + user_attributes["mail"][0] + "\n"

        cred_title = (
            user_attributes["displayName"][0]
            + " - "
            + user_attributes["userPrincipalName"][0]
        )

        if len(cred_title) > 64:
            cred_title = user_attributes["userPrincipalName"][0]
            if len(cred_title) > 64:
                cred_title = cred_title[0:63]

        cred_data = {
            "name": cred_title,
            "uri": "https://sso.srvfarm.net/",
            "username": user_attributes["sAMAccountName"][0],
            "description": description,
        }

        # check cred_id
        cred_id = None
        if "codeorange-pwmgr-id" in user_attributes:
            cred_id = user_attributes["codeorange-pwmgr-id"][0]
        else:
            cred_data["password_cleartext"] = "unknown"

        pwmgr_response = pwmgr_set_single_credentials(
            settings.MDAT_ROOT_CUSTOMER_ID,
            org_tag=org_tag,
            folder="SRVFARM",
            cred_data=cred_data,
            cred_id=cred_id,
        )

        if pwmgr_response:
            # get id from ['body']['id']
            ldap_con.modify(
                user_attributes["distinguishedName"][0],
                {
                    "codeorange-pwmgr-id": [
                        (MODIFY_REPLACE, pwmgr_response["body"]["id"])
                    ]
                },
            )

    return


@shared_task(name="directory_sync_guacamole_groups")
def directory_sync_guacamole_groups():
    ldap_con = ldap_connection()

    for customer in DirectoryTenantSettings.objects.all():
        tenant_ou = "ou=" + customer.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE
        remote_access_ou = "ou=Remote-Session-Access," + tenant_ou

        print(tenant_ou)

        if not ldap_con.search(
            search_base=tenant_ou,
            search_filter="(&(objectClass=computer)(objectClass=user)(objectClass=person))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            continue

        ldap_objects = ldap_con.response

        for computer in ldap_objects:
            computer_name = computer["attributes"]["sAMAccountName"][0]
            computer_name_normal = computer["attributes"]["cn"][0]
            computer_attributes = computer["attributes"]
            computer_dn = computer_attributes["distinguishedName"][0]

            print(
                "Processing sAMAccountName: "
                + computer_name
                + " (DN: "
                + computer_dn
                + ")"
            )

            # Get computer OS
            computer_os = "unknown"
            if "operatingSystem" in computer["attributes"]:
                computer_os = computer["attributes"]["operatingSystem"][0]

            # Only add RDP for Windows devices
            if "Windows".upper() in computer_os.upper():
                computer_group_name = (
                    customer.org_tag.upper()
                    + "_Remote_Access_".upper()
                    + computer_name_normal.upper()
                    + "_RDP".upper()
                )

                remote_access_group = (
                    "cn=" + computer_group_name + "," + remote_access_ou
                )

                try:
                    ldap_con.search(
                        search_base=remote_access_group,
                        search_filter="(objectClass=group)",
                        search_scope=BASE,
                        attributes=ALL_ATTRIBUTES,
                    )
                except LDAPNoSuchObjectResult:
                    # new credentials
                    ldap_con.add(
                        dn=remote_access_group,
                        attributes={
                            "objectClass": ["group", "guacConfigGroup"],
                            "description": computer_group_name,
                            "name": computer_group_name,
                            "sAMAccountName": computer_group_name,
                            "groupType": -2147483646,
                            "guacConfigProtocol": "rdp",
                        },
                    )

                # Computer hostname
                computer_fqdn = "dummy"

                if "dNSHostName" in computer["attributes"]:
                    computer_fqdn = computer["attributes"]["dNSHostName"][0]

                ldap_con.modify(
                    dn=remote_access_group,
                    changes={
                        "objectClass": [(MODIFY_REPLACE, ["group", "guacConfigGroup"])],
                        "sAMAccountName": [(MODIFY_REPLACE, [computer_group_name])],
                        "description": [(MODIFY_REPLACE, [computer_group_name])],
                        "groupType": [(MODIFY_REPLACE, ["-2147483646"])],
                        "guacConfigProtocol": [(MODIFY_REPLACE, ["rdp"])],
                        "guacConfigParameter": [
                            (
                                MODIFY_REPLACE,
                                [
                                    "hostname=" + computer_fqdn,
                                    "ignore-cert=true",
                                    "port=3389",
                                    "security=nla",
                                    "server-layout=de-de-qwertz",
                                    "domain=SRVFARM",
                                ],
                            )
                        ],
                    },
                )

    return


@shared_task(name="directory_qualify_remote_access_superadmins")
def directory_qualify_remote_access_superadmins():
    all_tenants_with_sadmins = DirectoryTenantSettings.objects.filter(
        super_admin__isnull=False, org_tag__isnull=False
    )

    ldap_con = ldap_connection()

    for tenant_with_sadmin in all_tenants_with_sadmins:
        tenant_ou = (
            "ou=" + tenant_with_sadmin.org_tag + "," + settings.HOSTING_LDAP_SEARCH_BASE
        )
        remote_access_ou = "ou=Remote-Session-Access," + tenant_ou

        if not ldap_con.search(
            search_base=tenant_ou,
            search_filter="(&(objectClass=user)(sAMAccountName="
            + tenant_with_sadmin.super_admin
            + "))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        superadmin_dn = ldap_objects[0]["dn"]

        if not ldap_con.search(
            search_base=tenant_ou,
            search_filter="(&(objectClass=group)(name=Admins@"
            + tenant_with_sadmin.org_tag
            + "))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        admin_group_dn = ldap_objects[0]["dn"]

        if not ldap_con.search(
            search_base=remote_access_ou,
            search_filter="(&(objectClass=group)(objectClass=guacConfigGroup))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            continue

        ldap_objects = ldap_con.response

        for remote_access_group in ldap_objects:
            ad_add_members_to_groups(ldap_con, superadmin_dn, remote_access_group["dn"])
            ad_add_members_to_groups(
                ldap_con, admin_group_dn, remote_access_group["dn"]
            )

    return


@shared_task(name="directory_sync_with_contracts")
def directory_sync_with_contracts():
    return


@shared_task(name="directory_normalize_phone")
def directory_normalize_phone():
    phone_attributes = (
        "facsimileTelephoneNumber",
        "homePhone",
        "mobile",
        "otherFacsimileTelephoneNumber",
        "otherHomePhone",
        "otherMobile",
        "otherTelephone",
        "telephoneAssistant",
        "telephoneNumber",
    )

    ldap_con = ldap_connection()

    for tenant in DirectoryTenantSettings.objects.filter(org_tag__isnull=False):
        org_tag = tenant.org_tag

        if not ldap_con.search(
            search_base=f"OU={org_tag}," + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(objectCategory=person))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            raise Exception("Lookup failed.")

        ldap_objects = ldap_con.response

        for user in ldap_objects:
            print(
                "Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0]
            )

            for phone_attribute in phone_attributes:
                if phone_attribute in user["attributes"]:
                    try:
                        number_obj = phonenumbers.parse(
                            user["attributes"][phone_attribute][0], None
                        )
                    except phonenumbers.NumberParseException:
                        ldap_con.modify(
                            user["dn"],
                            {
                                phone_attribute: [
                                    (
                                        MODIFY_DELETE,
                                        user["attributes"][phone_attribute][0],
                                    )
                                ]
                            },
                        )
                        continue

                    number_tidy = phonenumbers.format_number(
                        number_obj, phonenumbers.PhoneNumberFormat.INTERNATIONAL
                    )

                    if user["attributes"][phone_attribute][0] == number_tidy:
                        continue

                    ldap_con.modify(
                        user["dn"],
                        {phone_attribute: [(MODIFY_REPLACE, number_tidy)]},
                    )

    return


@shared_task(name="directory_prefill_mail")
def directory_prefill_mail():
    ldap_con = ldap_connection()

    for tenant in DirectoryTenantSettings.objects.filter(org_tag__isnull=False):
        org_tag = tenant.org_tag

        if not ldap_con.search(
            search_base=f"OU={org_tag}," + settings.HOSTING_LDAP_SEARCH_BASE,
            search_filter="(&(objectClass=user)(objectCategory=person)(!(mail=*)))",
            search_scope=ldap3.SUBTREE,
            attributes=ldap3.ALL_ATTRIBUTES,
            get_operational_attributes=True,
            size_limit=0,
        ):
            continue

        ldap_objects = ldap_con.response

        for user in ldap_objects:
            print(
                "Processing sAMAccountName: " + user["attributes"]["sAMAccountName"][0]
            )

            if "mail" in user["attributes"]:
                continue

            ldap_con.modify(
                user["dn"],
                {
                    "mail": [
                        (MODIFY_REPLACE, user["attributes"]["userPrincipalName"][0])
                    ]
                },
            )

    return
